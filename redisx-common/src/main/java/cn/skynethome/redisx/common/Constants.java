package cn.skynethome.redisx.common;

public class Constants
{
    public static final String SET="PUT";
    public static final String DEL="DEL";
    public static final String STRING="STR";
    public static final String OBJECT="OBJ";
    public static final int ECPIRATIONTIME=-1;
    public static final String GROUPID = "redisx";
    public static final short COMMAND_JOIN_GROUP=1;
    public static final short COMMAND_SENT_DATA=2;
}
